package com.journaler.fragment

import com.journaler.R

class ManualFragment:BaseFragment() {
    override val logTag = "Manual_Fragment"
    override fun getLayout() = R.layout.fragment_manual
}