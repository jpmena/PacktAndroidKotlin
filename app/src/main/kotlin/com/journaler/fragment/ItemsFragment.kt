package com.journaler.fragment

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AnimationSet
import android.view.animation.BounceInterpolator
import com.journaler.R
import com.journaler.activity.NoteActivity
import com.journaler.activity.TodoActivity
import com.journaler.model.MODE
import java.text.SimpleDateFormat
import java.util.*

class ItemsFragment: BaseFragment() {
    override val logTag = "Items_Fragments"
    override fun getLayout(): Int {
        return R.layout.fragment_items
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(getLayout(),container,false)
        val btn = view?.findViewById<FloatingActionButton>(R.id.new_item)
        btn?.setOnClickListener{
            anim(btn)
            val items = arrayOf(
                    getString(R.string.todos),
                    getString(R.string.notes)
            )
            val builder = AlertDialog.Builder(this@ItemsFragment.context).setTitle(R.string.choose_a_type)
                    .setCancelable(true)
                    .setOnCancelListener{
                        anim(btn)
                    }
                    .setItems(
                            items,
                            {_,which ->
                                when (which){
                                    0-> {
                                        openCreateTodo()
                                    }
                                    1 -> {
                                        openCreateNote()
                                    }
                                    else -> Log.e(logTag, "Unknown option selected [ $which ]")
                                }

                            }
                    )
            builder.show()
        }
        return view
    }

    private val NOTE_REQUEST = 0

    private fun openCreateNote() {
        val intent = Intent(context, NoteActivity::class.java)
        val data = Bundle()
        data.putInt(MODE.EXTRAS_KEYS, MODE.CREATE.mode)
        intent.putExtras(data)
        startActivityForResult(intent,NOTE_REQUEST)
    }

    private val TODO_REQUEST = 1

    private fun openCreateTodo() {
        val date = Date(System.currentTimeMillis())
        val dateFormat = SimpleDateFormat("MMM dd YYYY", Locale.ENGLISH)
        val timeFormat = SimpleDateFormat("MM:HH", Locale.ENGLISH)

        val intent = Intent(context, TodoActivity::class.java)
        val data = Bundle()
        data.putInt(MODE.EXTRAS_KEYS, MODE.CREATE.mode)
        data.putString(TodoActivity.EXTRA_DATE,dateFormat.format(date))
        data.putString(TodoActivity.EXTRA_TIME,timeFormat.format(date))
        intent.putExtras(data)
        startActivityForResult(intent, TODO_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode){
            TODO_REQUEST -> {
                if(resultCode == Activity.RESULT_OK){
                    Log.i(logTag, "We Created a TODO")
                } else {
                    Log.w(logTag, "We didnt' created a new TODO")
                }
            }
            NOTE_REQUEST -> {
                if(resultCode == Activity.RESULT_OK){
                    Log.i(logTag, "We Created a NOTE")
                } else {
                    Log.w(logTag, "We didnt' created a new NOTE")
                }
            }
        }
    }

    private fun anim(btn: FloatingActionButton, expand:Boolean=true){
        /*btn.animate().setInterpolator(BounceInterpolator())
                .scaleX(if(expand){1.5f}else{1.0f})
                .scaleY(if(expand){1.5f}else{1.0f})
                .setDuration(2000)
                .start()*/
        val animation1 = ObjectAnimator.ofFloat(btn, "ScaleX",if(expand) {1.5f} else {1.0f})
        animation1.duration = 2000
        animation1.interpolator = BounceInterpolator()

        val animation2 = ObjectAnimator.ofFloat(btn, "ScaleY",if(expand) {1.5f} else {1.0f})
        animation2.duration = 2000
        animation2.interpolator = BounceInterpolator()

        val animation3 = ObjectAnimator.ofFloat(btn, "alpha",if(expand) {0.3f} else {1.0f})
        animation3.duration = 500
        animation3.interpolator = AccelerateInterpolator()

        val set = AnimatorSet()
        set.play(animation1).with(animation2).before(animation3)
        set.start()
    }

}