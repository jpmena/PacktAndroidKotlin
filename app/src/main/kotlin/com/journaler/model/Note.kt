package com.journaler.model

import android.location.Location

class Note(
        var title:String,
        var message:String,
        var location:Location
): Entry(
        title, message,location
){
    override var id = 0L
}