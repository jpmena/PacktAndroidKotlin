package com.journaler.model

import android.location.Location

class Todo(
        var title:String,
        var message:String,
        var location: Location,
        var scheduledFor: Long
): Entry(
        title, message,location
){
    override var id = 0L
}