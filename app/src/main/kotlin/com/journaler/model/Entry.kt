package com.journaler.model

import android.location.Location
import com.journaler.database.DbModel

abstract class Entry(
        title: String,
        message: String,
        location: Location
): DbModel()