package com.journaler.database

import android.content.ContentValues
import android.location.Location
import android.util.Log
import com.journaler.model.Note
import com.journaler.model.Todo
import java.util.*

object Db {
    private val tag = "Db"
    private val version = 1
    private val name = "students"

    val NOTE = object : Crud<Note> {
        override fun insert(what: Note): Long {
            val inserted =  insert(listOf(what))
            if(!inserted.isEmpty()) return inserted[0]
            return 0
        }
        override fun insert(what: Collection<Note>): List<Long> {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            db.beginTransaction()
            var inserted = 0
            val items = mutableListOf<Long>()
            what.forEach { unitem ->
                val values = ContentValues()
                val TABLE = DbHelper.TABLE_NOTES
                values.put(DbHelper.COLUMN_TITLE, unitem.title)
                values.put(DbHelper.COLUMN_MESSAGE, unitem.message)
                values.put(DbHelper.COLUMN_LOCATION_LATITUDE, unitem.location.latitude)
                values.put(DbHelper.COLUMN_LOCATION_LONGITUDE, unitem.location.longitude)
                val id = db.insert(DbHelper.TABLE_NOTES, null, values)
                if (id > 0){
                    items.add(id)
                    Log.v(tag, "Entry ID assigned [$id]")
                    inserted++
                }
            }
            val success = what.size == inserted
            if (success){
                db.setTransactionSuccessful()
            } else {
                items.clear()
            }
            db.endTransaction()
            db.close()
            return  items
        }

        override fun update(what: Note): Int = update(listOf(what))

        override fun update(what: Collection<Note>): Int {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            db.beginTransaction()
            var updated = 0
            what.forEach { unitem ->
                val values = ContentValues()
                val TABLE = DbHelper.TABLE_NOTES
                values.put(DbHelper.COLUMN_TITLE, unitem.title)
                values.put(DbHelper.COLUMN_MESSAGE, unitem.message)
                values.put(DbHelper.COLUMN_LOCATION_LATITUDE, unitem.location.latitude)
                values.put(DbHelper.COLUMN_LOCATION_LONGITUDE, unitem.location.longitude)
                val id = db.update(DbHelper.TABLE_NOTES,values, DbHelper.ID+"=?", arrayOf(unitem.id.toString()))
                if (id > 0){
                    Log.v(tag, "Entry ID assigned [$id]")
                    updated++
                }
            }
            val success = what.size == updated
            if (success){
                db.setTransactionSuccessful()
            } else {
                updated = 0
            }
            db.endTransaction()
            db.close()
            return  updated
        }

        override fun delete(what: Note): Int = delete(listOf(what))

        override fun delete(what: Collection<Note>): Int {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            db.beginTransaction()
            var ids = StringBuilder()
            what.forEachIndexed { idx, unitem ->
                ids.append(unitem)
                if(idx < what.size - 1){
                    ids.append(", ")
                }
            }
            val table = DbHelper.TABLE_NOTES
            val statement = db.compileStatement("DELETE from $table WHERE ${DbHelper.ID} in ($ids)")
            val count = statement.executeUpdateDelete()
            val success = count > 0
            if (success){
                db.setTransactionSuccessful()
                Log.i(tag, "DELETE [SUCCESS] [ $count ] | [ $statement ]")
            } else {
                Log.w(tag, "DELETE [FAILURE] [ $count ] | [ $statement ]")
            }
            db.endTransaction()
            db.close()
            return  count
        }

        override fun select(arg: Pair<String, String>): List<Note> = select(listOf(arg))

        override fun select(args: Collection<Pair<String, String>>): List<Note> {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            var selection = StringBuilder()
            var selectionArgs = mutableListOf<String>()
            args.forEachIndexed{ idx,arg ->
                selection.append("${arg.first}=?")
                if (idx < args.size - 1){
                    selection.append(",")
                }
                selectionArgs.add(arg.second)
            }
            val result = mutableListOf<Note>()
            val cursor = db.query(true,DbHelper.TABLE_NOTES,null,selection.toString(),selectionArgs.toTypedArray(),
                    null, null, null, null, null)
            while (cursor.moveToNext()){
                val id = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.ID))
                val title = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_TITLE))
                val message = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_MESSAGE))
                val latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LATITUDE))
                val longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LONGITUDE))
                val location = Location("")
                location.latitude = latitude
                location.longitude = longitude
                val note = Note(title,message,location)
                result.add(note)
            }
            cursor.close()
            return result
        }

        override fun selectAll(): List<Note> {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            val result = mutableListOf<Note>()
            val cursor = db.query(true,DbHelper.TABLE_NOTES,null,null,null,
                    null, null, null, null, null)
            while (cursor.moveToNext()){
                val id = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.ID))
                val title = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_TITLE))
                val message = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_MESSAGE))
                val latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LATITUDE))
                val longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LONGITUDE))
                val location = Location("")
                location.latitude = latitude
                location.longitude = longitude
                val note = Note(title,message,location)
                result.add(note)
            }
            cursor.close()
            return result
        }
    }

    val TODO = object : Crud<Todo>{
        override fun insert(what: Todo): Long {
            val inserted =  insert(listOf(what))
            if(!inserted.isEmpty()) return inserted[0]
            return 0
        }
        override fun insert(what: Collection<Todo>): List<Long> {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            db.beginTransaction()
            var inserted = 0
            val items = mutableListOf<Long>()
            what.forEach { unitem ->
                val values = ContentValues()
                val TABLE = DbHelper.TABLE_NOTES
                values.put(DbHelper.COLUMN_TITLE, unitem.title)
                values.put(DbHelper.COLUMN_MESSAGE, unitem.message)
                values.put(DbHelper.COLUMN_LOCATION_LATITUDE, unitem.location.latitude)
                values.put(DbHelper.COLUMN_LOCATION_LONGITUDE, unitem.location.longitude)
                values.put(DbHelper.COLUMN_SCHEDULED,unitem.scheduledFor)
                val id = db.insert(DbHelper.TABLE_TODOS, null, values)
                if (id > 0){
                    items.add(id)
                    Log.v(tag, "Entry ID assigned [$id]")
                    inserted++
                }
            }
            val success = what.size == inserted
            if (success){
                db.setTransactionSuccessful()
            } else {
                items.clear()
            }
            db.endTransaction()
            db.close()
            return  items
        }

        override fun update(what: Todo): Int = update(listOf(what))

        override fun update(what: Collection<Todo>): Int {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            db.beginTransaction()
            var updated = 0
            what.forEach { unitem ->
                val values = ContentValues()
                val TABLE = DbHelper.TABLE_NOTES
                values.put(DbHelper.COLUMN_TITLE, unitem.title)
                values.put(DbHelper.COLUMN_MESSAGE, unitem.message)
                values.put(DbHelper.COLUMN_LOCATION_LATITUDE, unitem.location.latitude)
                values.put(DbHelper.COLUMN_LOCATION_LONGITUDE, unitem.location.longitude)
                values.put(DbHelper.COLUMN_SCHEDULED,unitem.scheduledFor)
                val id = db.update(DbHelper.TABLE_TODOS, values, DbHelper.ID+"=?", arrayOf(unitem.id.toString()))
                if (id > 0){
                    Log.v(tag, "Entry ID assigned [$id]")
                    updated++
                }
            }
            val success = what.size == updated
            if (success){
                db.setTransactionSuccessful()
            } else {
                updated = 0
            }
            db.endTransaction()
            db.close()
            return  updated
        }

        override fun delete(what: Todo): Int = delete(listOf(what))

        override fun delete(what: Collection<Todo>): Int {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            db.beginTransaction()
            var ids = StringBuilder()
            what.forEachIndexed { idx, unitem ->
                ids.append(unitem)
                if(idx < what.size - 1){
                    ids.append(", ")
                }
            }
            val table = DbHelper.TABLE_TODOS
            val statement = db.compileStatement("DELETE from $table WHERE ${DbHelper.ID} in ($ids)")
            val count = statement.executeUpdateDelete()
            val success = count > 0
            if (success){
                db.setTransactionSuccessful()
                Log.i(tag, "DELETE [SUCCESS] [ $count ] | [ $statement ]")
            } else {
                Log.w(tag, "DELETE [FAILURE] [ $count ] | [ $statement ]")
            }
            db.endTransaction()
            db.close()
            return  count
        }

        override fun select(arg: Pair<String, String>): List<Todo> = select(listOf(arg))

        override fun select(args: Collection<Pair<String, String>>): List<Todo> {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            var selection = StringBuilder()
            var selectionArgs = mutableListOf<String>()
            args.forEachIndexed{ idx,arg ->
                selection.append("${arg.first}=?")
                if (idx < args.size - 1){
                    selection.append(",")
                }
                selectionArgs.add(arg.second)
            }
            val result = mutableListOf<Todo>()
            val cursor = db.query(true,DbHelper.TABLE_TODOS,null,selection.toString(),selectionArgs.toTypedArray(),
                    null, null, null, null, null)
            while (cursor.moveToNext()){
                val id = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.ID))
                val title = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_TITLE))
                val message = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_MESSAGE))
                val latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LATITUDE))
                val longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LONGITUDE))
                val location = Location("")
                location.latitude = latitude
                location.longitude = longitude
                val scheduled = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_SCHEDULED)) // Timestamp
                val todo = Todo(title,message,location,scheduled)
                result.add(todo)
            }
            cursor.close()
            return result
        }

        override fun selectAll(): List<Todo> {
            val db = DbHelper(name, version).writableDatabase //it opens the Database
            val result = mutableListOf<Todo>()
            val cursor = db.query(true,DbHelper.TABLE_TODOS,null,null,null,
                    null, null, null, null, null)
            while (cursor.moveToNext()){
                val id = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.ID))
                val title = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_TITLE))
                val message = cursor.getString(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_MESSAGE))
                val latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LATITUDE))
                val longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_LOCATION_LONGITUDE))
                val location = Location("")
                location.latitude = latitude
                location.longitude = longitude
                val scheduled = cursor.getLong(cursor.getColumnIndexOrThrow(DbHelper.COLUMN_SCHEDULED)) // Timestamp
                val todo = Todo(title,message,location,scheduled)
                result.add(todo)
            }
            cursor.close()
            return result
        }
    }
}