package com.journaler.database

interface Crud<T> where T:DbModel {
    companion object { //serves only to define constants !!!
        val BROADCAST_ACTION = "com.journaler.broadcast.crud"
        val BROADCAST_EXTRAS_KEY_CRUD_OPERATION_RESULT = "crud_result"
    }

    /**
     * Returns the id of the inserted object
     */
    fun insert(what: T): Long

    /**
     * Returns the list of the inserted objects
     * through their ids !!!
     */
    fun insert(what: Collection<T>): List<Long>

    /**
     * Returns number of updated items
     */
    fun update(what: T): Int

    /**
     * Returns number of updated items
     */
    fun update(what: Collection<T>): Int

    /**
     * Returns number of deleted items
     */
    fun delete(what: T): Int

    /**
     * Returns number of deleted items
     */
    fun delete(what: Collection<T>): Int

    /**
     * Returns the list of items
     * Pair: column name, value in the where condition  ?
     */
    fun select(what: Pair<String, String>): List<T>

    /**
     * Returns the list of items
     */
    fun select(what: Collection<Pair<String, String>>): List<T>

    /**
     * Returns the list of items
     */
    fun selectAll():List<T>
}