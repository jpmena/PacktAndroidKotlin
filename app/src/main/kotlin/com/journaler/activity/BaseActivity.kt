package com.journaler.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.journaler.R
import com.journaler.permission.PermissionCompactActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

abstract class BaseActivity : PermissionCompactActivity(){

    companion object {

        val REQUEST_GPS = 0;

        private var fontExoBold: Typeface? = null
        private var fontExoRegular: Typeface? = null

        fun applyFonts(view: View, ctx: Context) {
            var vTag = ""
            if (view.tag is String) {
                vTag = view.tag as String
            }
            when (view) {
                is ViewGroup -> {
                    for (x in 0..view.childCount - 1) {
                        applyFonts(view.getChildAt(x), ctx)
                    }
                }
                is Button -> {
                    when (vTag) {
                        ctx.getString(R.string.tag_font_bold) -> {
                            view.typeface = fontExoBold
                        }
                        else -> {
                            view.typeface = fontExoRegular
                        }

                    }
                }
                is TextView -> {
                    when (vTag) {
                        ctx.getString(R.string.tag_font_bold) -> {
                            view.typeface = fontExoBold
                        }
                        else -> {
                            view.typeface = fontExoRegular
                        }

                    }
                }
                is EditText -> {
                    when (vTag) {
                        ctx.getString(R.string.tag_font_bold) -> {
                            view.typeface = fontExoBold
                        }
                        else -> {
                            view.typeface = fontExoRegular
                        }

                    }
                }
            }
        }
    }

    protected abstract val tag: String
    protected abstract fun getLayout():Int
    protected abstract fun getActivityTitle(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        setContentView(getLayout())
        //activity_title.setText(getActivityTitle())
        setSupportActionBar(toolbar)
        //requestGpsPermissions() replaced by
        requestPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)
        Log.d(tag, "[ ON CREATE 1]")
    }

    /*private fun requestGpsPermissions() {
        ActivityCompat.requestPermissions(this@BaseActivity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_GPS)
    }*/

    /* Replaces by the result of the parent Class (already overriden)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == REQUEST_GPS){
            for (grantResult in grantResults){
                if(grantResult == PackageManager.PERMISSION_GRANTED){
                    Log.i(tag, String.format(Locale.ENGLISH, "Permission granted [%d]", requestCode))
                } else {
                    Log.i(tag, String.format(Locale.ENGLISH, "Permission not granted [%d]", requestCode))
                }
            }
        }
    }*/

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        Log.d(tag, "[ ON POST CREATE ]")
        applyFonts()
    }
    override fun onRestart() {
        super.onRestart()
        Log.d(tag, "[ ON RESTART ]")
    }
    override fun onStart() {
        super.onStart()
        Log.d(tag, "[ ON START ]")
    }
    override fun onResume() {
        super.onResume()
        Log.d(tag, "[ ON RESUME ]")
        val animation = getAnimation(R.anim.top_to_bottom);
        findViewById<View>(R.id.toolbar).startAnimation(animation)
    }
    override fun onPostResume() {
        super.onPostResume()
        Log.d(tag, "[ ON POST RESUME ]")
    }
    override fun onPause() {
        super.onPause()
        Log.d(tag, "[ ON PAUSE ]")
        val animation = getAnimation(R.anim.hide_to_top);
        findViewById<View>(R.id.toolbar).startAnimation(animation)
    }
    override fun onStop() {
        super.onStop()
        Log.d(tag, "[ ON STOP ]")
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.d(tag, "[ ON DESTROY ]")
    }
    protected fun applyFonts(){
        initFonts()
        Log.d(tag, "Applying fonts [START]")
        val rootView = findViewById<View>(android.R.id.content)
        applyFonts(rootView, this)
        Log.d(tag, "Applying fonts [END]")
    }
    fun Activity.getAnimation(animation:Int):Animation =
            AnimationUtils.loadAnimation(this,animation)
    private fun initFonts(){
        if (fontExoBold == null){
            Log.d(tag, "Initializing font [Exo2Bold]")
            fontExoBold = Typeface.createFromAsset(assets, "fonts/Exo2-Bold.ttf")
        }
        if (fontExoRegular == null){
            Log.d(tag, "Initializing font [Exo2Regular]")
            fontExoRegular = Typeface.createFromAsset(assets, "fonts/Exo2-Regular.ttf")
        }
    }
}