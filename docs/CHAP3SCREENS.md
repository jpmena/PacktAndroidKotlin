# My notes of chapter 3 of [Mastering Android Development with Kotlin by Packt](https://www.packtpub.com/application-development/mastering-android-development-kotlin)

* Starting at page 73 !!!
## Mockup using [Pencil](https://pencil.evolus.vn/)

* [Pencil](https://pencil.evolus.vn/) has a rpm package ....
    * The [FireFox Extension](https://addons.mozilla.org/fr/firefox/addon/pencil/) is tool old for my 58 / Fedora version!
* It exists as dnf package but has [a known problem with that old packaged version](https://bugzilla.redhat.com/show_bug.cgi?id=1503628)
* So for my [Fedora 27](https://getfedora.org/)use the [64Bits rpm provided](https://pencil.evolus.vn/dl/V3.0.4/Pencil-3.0.4-49.x86_64.rpm) _(53 MBytes)_

```bash
[jpmena@localhost Ateliers]$ sudo rpm -ivh Pencil-3.0.4-49.x86_64.rpm
[sudo] Mot de passe de jpmena : 
Préparation...                       ################################# [100%]
Mise à jour / installation...
   1:Pencil-3.0.4-49                  ################################# [100%]
```

# What is expected 

![App schema](TODOApp.jpg)

* p 77 : looking for the difference on *app/build.gradle* between
    * my app

```groovy
dependencies {
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version"
    implementation 'com.android.support:design:26+'
    implementation 'com.android.support:appcompat-v7:26.1.0'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.android.support.test:runner:1.0.1'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.1'


    compile "com.google.code.gson:gson:2.8.0"
    //compile "com.github.salomonbrys.kotson:kotson:2.3.0"
    compile "com.squareup.retrofit2:retrofit:2.3.0"
    compile "com.squareup.retrofit2:converter-gson:2.0.2"
    compile "com.squareup.okhttp3:okhttp:3.9.0"
    compile "com.squareup.okhttp3:logging-interceptor:3.9.0"
} 

```
* And the [Packt's GitHub Solution]()

```groovy
dependencies {
    compile "org.jetbrains.kotlin:kotlin-reflect:1.1.51"
    compile "org.jetbrains.kotlin:kotlin-stdlib:1.1.51"
    compile "com.android.support:design:26.0.1"
    compile "com.android.support:appcompat-v7:26.0.1"

    compile "com.google.code.gson:gson:2.8.0"
    compile "com.github.salomonbrys.kotson:kotson:2.3.0"
    compile "com.squareup.retrofit2:retrofit:2.3.0"
    compile "com.squareup.retrofit2:converter-gson:2.0.2"
    compile "com.squareup.okhttp3:okhttp:3.9.0"
    compile "com.squareup.okhttp3:logging-interceptor:3.9.0"

    compile "junit:junit:4.12"
    testCompile "junit:junit:4.12"

    testCompile "org.jetbrains.kotlin:kotlin-reflect:1.1.51"
    testCompile "org.jetbrains.kotlin:kotlin-stdlib:1.1.51"

    compile "org.jetbrains.kotlin:kotlin-test:1.1.51"
    testCompile "org.jetbrains.kotlin:kotlin-test:1.1.51"

    compile "org.jetbrains.kotlin:kotlin-test-junit:1.1.51"
    testCompile "org.jetbrains.kotlin:kotlin-test-junit:1.1.51"

    compile 'com.android.support:support-annotations:26.0.1'
    androidTestCompile 'com.android.support:support-annotations:26.0.1'

    compile 'com.android.support.test:runner:0.5'
    androidTestCompile 'com.android.support.test:runner:0.5'

    compile 'com.android.support.test:rules:0.5'
    androidTestCompile 'com.android.support.test:rules:0.5'


}
```

p 90: weight Sum is the Sum we expect of the children weightsem 

## Understanding Android Context p 93:

## Fragments p 94

* p 98: I have great difficulties with Tagging Log messages and filtering LogCat !!!
  * I had to avoid blanks in the Tag
  * Still not seeinf MainActivity
    * the solution change Log.v for Log.d to be at the same level as the Fragment
  * The Tag RegExp _Main_Activity|Items_Fragments_
    * if you put [] each character is looked for in the logs !!!
    
* p 100: Fragment and backspace workking. We can replace at no cost !!!
  * On logcat you have to add the Lorem Fragment to the RegExp filter
  * it becomes _Main_Activity|Items_Fragments|Manual_Fragment_
  
### Fragments and adapter

* our pager accepts the previous FragmentManager (as constructor / argument ) that dealt with transition generation !!!!

### Making animations with transitions p 103
