# le 09/06/2018

* p 182 pas 

# le 28/07/2018

* lien intéressant pour expliquer un object Kotlin
  * <https://www.programiz.com/kotlin-programming/object-singleton>
* Ce lien [d'une entreprise de technologie](https://www.odelia-technologies.com/blog/object-companion-kotlin.html)
  * explique l'intérêt du cmot Clé Companion
  * intérêt d'un companion dans une classe à travers l'exemple de Student !!!!
  * dans notre cas j'accède _NODE.la_ConstanteDuCompanionObject_
  
* Dans notre Cas DBHelper est un sigleton qui contient d'autres Singletons 
  * Db.NOTE
  * Db.TODO
  
# page 183 

* locationListener est un une inner class singleton (implémente LocationProvider) de la classe parente (également singleton)
  * object LocationProvider 
  * la inner classe accède aux attributs de la upperclasse
  * Elle sert ici à agglomérer les Location Provider