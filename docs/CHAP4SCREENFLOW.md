#p 109: TODO replacement buttons by a mmenu !!!
# p 118: TODO remembering how ItemFragments is called !!!!
* At that time Fragment do not seem to be used !!!
* Only the ItemsFragment is called through the Pager, private class _ViewPagerAdpater_ (line 75 of _MainActivity_)
 * each time we swipe, a new view is called and each time it is a itemsFragment: 

```kotlin
private class ViewPagerAdpater(manager:FragmentManager):FragmentStatePagerAdapter(manager){
        override fun getItem(position: Int): Fragment {
            return ItemsFragment()
        }

        override fun getCount(): Int {
            return 5
        }
 }
```

* The pager instance whose adapter is the above private Class is attached to the MainActivity 
  * in _activity_main.xml_
* pager is member of the MainActivity Class whose adapter is the above class !!!

```xml
<android.support.v4.view.ViewPager
        xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_height="match_parent"
        android:layout_width="match_parent"
        android:id="@+id/pager" />
```

## Working with intents p 124

* What is the second parameter of [startActivityForResult](https://developer.android.com/reference/android/app/Activity#startActivityForResult(android.content.Intent,%20int))
  * The answer is in the link
  * it is the code that _onActivityResult()_ expects as REquest Parameter 
    * in order to know which source
    
## p 126 The common intented ItemActivity

*  _ItemActivity_ is the parent activity of the 2 targets of the intent
  * target _class NoteActivity: ItemActivity()_
  * target _class TodoActivity: ItemActivity()_
  
